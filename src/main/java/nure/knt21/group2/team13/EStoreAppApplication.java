package nure.knt21.group2.team13;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EStoreAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(EStoreAppApplication.class, args);
    }

}
